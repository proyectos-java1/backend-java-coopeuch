package cl.test.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import cl.test.api.config.SwaggerConfig;
import cl.test.api.model.entity.DatosAlumno;
import cl.test.api.service.IDatosAlumnoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;


@CrossOrigin(origins = {"http://127.0.0.1:5173", "http://localhost:3000"})
@Api(value = "Controlador Detalle de Productos")
@Import(SwaggerConfig.class)
@RestController
@RequestMapping("/api/v1")
public class MantenedorController {

	Logger log = LoggerFactory.getLogger(MantenedorController.class);

	@Autowired
	private IDatosAlumnoService datosAlumnoService;


		@ApiOperation(value = "Lista completa Detalle de Alumnos", response = MantenedorController.class)
		@GetMapping("/getListaAlumnos")
		public List<DatosAlumno> getListaAlumnos(){

			try {
				if (datosAlumnoService.findAll().isEmpty()){
					log.error("No se encuentran datos!!");
				}
				return datosAlumnoService.findAll();
			} catch (Exception ex) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo encontrar Datos!", ex);
			}

		}

		@ApiOperation(value = "Entre el detalle del Alumnos por ID", response = MantenedorController.class)
		@GetMapping("/getAlumnoId/{id}")
		public DatosAlumno getAlumnoXId(@PathVariable Long id) {

			try {
				if (id == null){
					log.error("Dato ID vacio!!");
				}
				return datosAlumnoService.findById(id);

			} catch (Exception ex) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo encontrar Datos!", ex);
			}
		}
		@ApiOperation(value = "Agregar nuevo Estatus de Alumno", response = MantenedorController.class)
		@PostMapping("/addNewAlumno")
		@ResponseStatus(HttpStatus.CREATED)
		public DatosAlumno addNewAlumno(@RequestBody DatosAlumno addNewAlumno) {

			try {
				if (addNewAlumno == null) {
					log.error("Error con los datos de entrada");
				}
				return datosAlumnoService.save(addNewAlumno);
			} catch (Exception ex) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo grabar los Datos!", ex);
			}
		}

		@ApiOperation(value = "Actualiza Estatus de Alumno", response = MantenedorController.class)
		@PostMapping("/updateAlumno/{id}")
		@ResponseStatus(HttpStatus.CREATED)
		public DatosAlumno updateAlumno(@RequestBody DatosAlumno datoAlumno, @PathVariable Long id) {

				DatosAlumno datoAlumnoUpdate = datosAlumnoService.findById(id);

			try {
				if (id == null){
					log.error("Error con los datos de entrada");
				}
				datoAlumnoUpdate.setDescripcion(datoAlumno.getDescripcion());
				datoAlumnoUpdate.setVigente(datoAlumno.getVigente());

				return datosAlumnoService.save(datoAlumnoUpdate);

			} catch (Exception ex) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo actualizar los Datos!", ex);
			}
		}

		@ApiOperation(value = "Borra estatus de alumno por ID", response = MantenedorController.class)
		@DeleteMapping("/deleteAlumno/{idAlumno}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void deleteAlumno(@PathVariable Long idAlumno) {

			try {
				datosAlumnoService.delete(idAlumno);
			} catch (Exception ex) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo borrar Datos!", ex);
			}
		}


	}
