package cl.test.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.test.api.model.entity.DatosAlumno;
import cl.test.api.model.dao.IDatosAlumnoDao;
import cl.test.api.service.IDatosAlumnoService;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DatosAlumnosServiceImpl implements IDatosAlumnoService{
	@Autowired
	private IDatosAlumnoDao datosAlumnosDao;


	@Override
	@Transactional(readOnly = true)
	public List<DatosAlumno> findAll() {
		return (List<DatosAlumno>) datosAlumnosDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public DatosAlumno findById(Long id) {
		return datosAlumnosDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<DatosAlumno> findByIdentificador(Integer alumnoId) {
		return Optional.ofNullable(datosAlumnosDao.findByIdentificador(alumnoId));
	}

	@Override
	@Transactional
	public DatosAlumno save(DatosAlumno datosDeAlumno) {
		return datosAlumnosDao.save(datosDeAlumno);
	}

	@Override
	public void delete(Long id) { datosAlumnosDao.deleteById(id); }

}
