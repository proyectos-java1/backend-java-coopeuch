package cl.test.api.service;

import java.util.List;
import java.util.Optional;

import cl.test.api.model.entity.DatosAlumno;

public interface IDatosAlumnoService {
	
	List<DatosAlumno> findAll();
	
	DatosAlumno findById(Long id);

	Optional<DatosAlumno> findByIdentificador(Integer id);
	
	DatosAlumno save(DatosAlumno datosDeAlumno);
	
	void delete(Long id);
	

}
