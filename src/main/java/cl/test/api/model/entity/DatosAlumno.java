package cl.test.api.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "detalle_alumnos")
public class DatosAlumno implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer identificador;

	private String descripcion;

	@NotNull
	@Temporal(TemporalType.DATE)
	private Date fechaCreacion;
	
	private Boolean vigente;


	@PrePersist
	public void preSave() {
		fechaCreacion = new Date();
	}

}
