package cl.test.api.model.dao;

import org.springframework.data.repository.CrudRepository;

import cl.test.api.model.entity.DatosAlumno;


public interface IDatosAlumnoDao extends CrudRepository <DatosAlumno, Long> {
	
	DatosAlumno findByIdentificador(Integer alumnoId);

}
