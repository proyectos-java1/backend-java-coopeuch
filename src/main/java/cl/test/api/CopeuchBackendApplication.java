package cl.test.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"cl.test.api"})
public class CopeuchBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CopeuchBackendApplication.class, args);
	}

}
