insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (10, 'Alumno cursando 1er año de Fotografía', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (11, 'Alumno cursando 3er año de Comercio Exterior', NOW(), false);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (12, 'Alumno cursando 5to año de Ingenieria Civil', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (13, 'Alumno cursando 3er año de Construcción Civil', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (14, 'Alumno cursando 2do año de Fotografía', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (15, 'Alumno cursando 1er año de Soporte Informático', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (16, 'Alumno cursando 3er año de Diseño Gráfico', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (17, 'Alumno cursando 5to año de Enfermería', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (18, 'Alumno cursando 1er año de Contabilidad', NOW(), true);
insert into detalle_alumnos (identificador, descripcion, fecha_creacion, vigente)
values (19, 'Alumno cursando 1er año de Diseño Web', NOW(), true);