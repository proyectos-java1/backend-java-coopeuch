package cl.test.api;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import cl.test.api.service.IDatosAlumnoService;

class CopeuchBackendApplicationTest {

    IDatosAlumnoService interfaceDatosAlumnoService;

    @BeforeEach
    void setUp() {
        interfaceDatosAlumnoService = mock(IDatosAlumnoService.class);
    }

    @Test
    void contextLoads() {

        when(interfaceDatosAlumnoService.findById(1L)).thenReturn(Datos.ALUMNO_01);
        when(interfaceDatosAlumnoService.findById(2L)).thenReturn(Datos.ALUMNO_02);
        when(interfaceDatosAlumnoService.findById(3L)).thenReturn(Datos.ALUMNO_03);
    }
}